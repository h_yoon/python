
from functools import wraps

#학습 목표: 래핑 기능 확인.
#주요 내용: 래핑 기능 이해.
#         @함수명 이해.
#wraps   사용: 함수 속성 유지 O
#        미사용: 함수 속성 유지 X

def my_deco1(func):
    def run_func():
        print(__name__, "랩 미 사용")
        func()
    return run_func

def my_deco2(func):
    @wraps(func) # 랩 사용
    def run_func():
        print(__name__, "랩 사용")
        func()
    return run_func

@my_deco1
def my_func1():
    '''
    func attr text1 >> 래핑 미사용. my_deco1를 데코레이팅하지만(@사용), 
    래핑하지 않음으로 이구문은 출력돼지 않는다.
    '''
    print("my func1 run")

@my_deco2
def my_func2():
    '''
    func attr text2 >> 래핑 사용. 
    my_deco2 함수를 데코레이팅 한다. my_deco2() 수행후, my_func2 내용이 출력된다.
    = my_deco2함수를 래핑하여 my_deco2의 추가동작 정의
    '''
    print("my_func2 run")


my_func1()
print("Func1 Name :", my_func1.__name__)
print("Func1 Doc :", my_func1.__doc__)

print('-' * 50)

my_func2()
print("Func2 Name:", my_func2.__name__)
print("Func1 Doc:", my_func2.__doc__)

deco_obj1 = my_deco1(my_func1)
deco_obj1()

print('-' * 50)
deco_obj2 = my_deco1(my_func2)
deco_obj2()
