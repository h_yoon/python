
from abc import ABC, abstractmethod

class Template(ABC):
    def __init__(self):
        pass
    
    @abstractmethod
    def func1(self):
        pass
    
    @abstractmethod
    def func2(self):
        pass

    @staticmethod
    def comm_func():
        print("Run func1 and func2.")
    
    def execute(self):
        self.comm_func()
        self.func1()
        self.func2()
        
class TemplateClass1(Template):
    def func1(self):
        print("template func 1 called")
    
    def func2(self):
        print("template mehtd 2 called")

class TemplateClass2(Template):
    def func1(self):
        print("template calss 2 func 1().")
    
    def func2(self):
        print("template class 2 func2()")

tem1 = TemplateClass1()
tem1.execute()

tem2 = TemplateClass2()
tem2.execute()

