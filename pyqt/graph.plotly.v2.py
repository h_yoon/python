
import pyqtgraph as pg

from PyQt5.QtWidgets import *
from PyQt5.QtCore import pyqtSlot, pyqtSignal, QObject, Qt, QThread, QTimer

import time, random

class Window(QWidget):
    def __init__(self):
        super().__init__()
        
        hbox = QHBoxLayout()
        self.monitor1 = pg.PlotWidget(title="process monitor")
        
        hbox.addWidget(self.monitor1)
        self.setLayout(hbox)
        
        self.setWindowTitle("process monitor")
        
        self.data_x = [1, 2, 3]
        self.data_y = [3, 2, 1]
        
        self.pl = self.monitor1.plot(pen='g')
        
        self.mytimer = QTimer()
        self.mytimer.start(1000)
        self.mytimer.timeout.connect(self.get_data)
        
        self.draw_chart(self.data_x, self.data_y)
        self.show()
        
    def draw_chart(self, x, y):
        self.pl.setData(x=x, y=y)
            
        self.monitor1.clear()
        bar_chart = pg.BarGraphItem(x=x, height=y, width=12, brush='y', pen='r')
        self.monitor1.addItem(bar_chart)
        
    @pyqtSlot()    
    def get_data(self):
        data: str = time.strftime("%S", time.localtime()) # 초단위만
        
        last_x = self.data_x[-1]
        self.data_x.append(last_x + 1)
        self.data_y.append(random.random() * 60)
        
        self.draw_chart(self.data_x, self.data_y)
        
if __name__ == "__main__":
    import sys
    
    app = QApplication(sys.argv)
    
    ex = Window()
    
    sys.exit(app.exec_())        
        