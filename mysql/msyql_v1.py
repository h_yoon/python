
#ref https://yurimkoo.github.io/python/2019/09/14/connect-db-with-python.html
# mysql 접근 예제1

import pymysql

local_db = pymysql.connect(
    user = 'root',
    passwd = 'hs',
    host='127.0.0.1',
    db='hyoon_wiki',
    charset='utf8'
)

#connect
cursor = local_db.cursor(pymysql.cursors.DictCursor)

# select
sql = "SELECT * FROM `actor`;"
cursor.execute(sql)
result = cursor.fetchall()

import pandas as pd

result = pd.DataFrame(result)
result

print(result)

# 조작

# delete