import time

start_time = time.time()

# 멀티 스레드 미사용, 20만 카운트
def count(name):
    for i in range(1, 50001):
        print(name," : ", i)
    
num_list = ['p1', 'p2', 'p3', 'p4']

# for num in num_list:
#     count(num)
    
# print(" end %s seconds" % (time.time() - start_time))


import multiprocessing

start_time = time.time()

#멀티 스레드 사용

if __name__ == '__main__':
    pool = multiprocessing.Pool(processes=2)
    pool.map(count, num_list) # functioin, iterable
    pool.close()
    pool.join()
print(" multi processing end % seconds " %(time.time() - start_time))

