

from abc import ABC, abstractmethod

class DBManager(ABC):
    @abstractmethod
    def connection(self):
        pass

class SqlServer(DBManager):
    def connection(self):
        return ("Microsoft Database Connected")

class Oracle(DBManager):
    def connection(self):
        return ("Oracle Database Connected")

class DbConnectionFactory:
    def get_db_connection(self, db):
        return db.connection()

db_fact = DbConnectionFactory()

print(db_fact.get_db_connection(SqlServer()))
print(db_fact.get_db_connection(Oracle()))