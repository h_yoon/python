
import math
n = math.sqrt(9.0)

print(n)

i=1
while i <= 10:
    print(i)
    i += i

print("----------------")
sum = 0
for i in range(12):
    sum += i
    print(sum)

print("-----------------")
list = ["this", "is", "a", "book"]  # list
for s in list:
    print(s)
print("-----------------")
set = {"this", "is", "a", "book"} # set
for s in set:
    print(s)
print("-----------------")

# function
def sum(a,b):
    s = a +b
    return s

total = sum(4,1)
print(total)

print("-----------------")

def f (i, mylist):
    i = i + 1
    mylist.append(123)

k = 10
m = [ 1,2,3,4]
f(k, m)
print(k, m) # 함수 인자는 k와 m에 대해 레퍼런스로 전달됨.
print("-----------------")

#가변길이 인자
def total(*numbers):
    tot = 0
    for n in numbers:
        tot += n
    return tot

t = total(1,2,3)
print(t)
t = total(1,5,2,5)
print(t)
print("-----------------")
#복수개 리턴
def calc(*numbers) :
    cnt =0
    tot = 0
    for n in numbers:
        cnt += 1
        tot += n
    return cnt, tot

count, sum = calc(1,56,23,4,23,5)
print(count, sum)
print("-----------------")
#class
class Ractangle1:
    count = 0
    
    def __init__(self, width, height):
        self.width = width
        self.height = height
        self.count += 1
    
    def calc_area(self):
        area = self.width * self.height
        return area

print("-----------------")
class Ractangle2:
    count = 0 
    
    def __init__ (self, w, h):
        self.width = w
        self.height = h
        self.count += 1
    def calc_area(self):
        area = self.width * self.height
        return area

    #static method
    @staticmethod
    def is_square(w, h):
        return w == h
    
    #class method
    @classmethod
    def print_count(cls): #cls는 객체인스턴스를 의미로 self대신 사용. 클래스를 의미.
        print(cls.count)
        
square = Ractangle2.is_square(5,5)
print(square)
r1 = Ractangle2(5,5)
r2 = Ractangle2(1,5)
r1.print_count()
r2.print_count()
print("-----------------")
#상속
class Animal :
    def __init__(self, name):
        self.name = name
    def move(self):
        print("move")
    def speak(self):
        pass

class Dog (Animal):
    def speak(self):
        print("멍멍")
    
class Horse (Animal):
    def speak(self):
        print("히히힝")
        
dog = Dog("dobby")
n = dog.name
dog.move()
dog.speak()
print(n)

print("-----------------")

print("-----------------")
#예외처리
def calc(values):
    sum = None
    try:
        sum = values[0] + values[1] + values[2]
    except IndexError as err:
        print("인덱스 에러")
    except Exception as err:
        print(str(err))
    else:
        print("에러 없음")
    finally:
        print(sum)

test_list1 = [1,2]
calc(test_list1)

print("-----------------")
#반복자

print("-----------------")
#Generator

print("-----------------")
#스레드
